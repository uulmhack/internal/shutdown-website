# uulmhack shutdown website

[See live](https://uulmhack.dev)

Static website for displaying the shutdown info text as well as pages for contact, imprint and privacy policy, each in German and English.

The HTML code and assets were extracted from the Typo3 page and only slightly adjusted to make it work.
